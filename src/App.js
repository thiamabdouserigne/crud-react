import React, { useEffect } from 'react';
import './App.css';
 import UserTable from './tables/UserTable';
import userList from './data';
import { useState } from 'react';
import AddUserForm from './forms/AddUserForm';
import EditUser from './forms/EditUser';


function App() {
  const [users, setUsers] = useState(userList)
  const [data, setData] = useState([])


  useEffect(() => {
    const usersid = JSON.parse(localStorage.getItem('users'));
    if (usersid) {
      setData(usersid)
    }
}, []);
  const addUser = user => {
    user.id = users.length + 1 ;
    setUsers([...users,user])
  }

  const deleteUser = id => setUsers(users.filter(user => user.id !== id ))
  

  const [editing, setEditing] = useState(false);

  const initialUser = {
      prenom:'',
      nom: '',
      email: '',
      phone: ''
  };

  const [currentUser, setCurrentUser]= useState(initialUser)

  const editUser = (id,user) => {
    setEditing(true)
    setCurrentUser(user)
  }

  const updateUser = (newUser) => {
    setUsers(users.map(user => (user.id === currentUser.id ? newUser : user )))
  } 
  return (
    <div className="App">
  {
      editing ? (
        <EditUser 
           currentUser= {currentUser}
           updateUser= {updateUser}
        />
      ) : (
        <AddUserForm  addUser={addUser}/>
      )
      }
    
    <hr/>
    
    <UserTable users={data} deleteUser={deleteUser} editUser={editUser}/>
    </div>
);

}
export default App
