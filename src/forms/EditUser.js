import React,{useEffect, useState}from "react";

const EditUser = (props) => {

    const [user,setUser] = useState(props.currentUser)

    const handleChange = e => {
        const {name, value} = e.target;
        setUser({...user,[name]: value})
    }

    const handleSubmit = e => {
        e.preventDefault();
        if(user){
            handleChange(e, props.updateUser(user))
        }
    }

    useEffect(() => {
    setUser(props.currentUser)
}, [props])

    return(
        <div className="form">
                <h2>Jeemacoder Gestion d'utilisateurs</h2>
            <form id="form" className="shadow ">
                <div className="form-group d-flex">
                    <label htmlFor="prenom" className="form-label">Prenom</label>
                    <input className="form-control" type='text' id="prenom" name='prenom' value={user.prenom} onChange={handleChange} />
                    <label htmlFor="nom" className="form-label">Nom</label>
                    <input className="form-control" type='text' id="nom" name='nom' value={user.nom} onChange={handleChange}/>
                </div>
                <div className="form-group d-flex ">
                    <label htmlFor="email" className="form-label">Email</label>
                    <input className="form-control" type='email' id="email" name='email' value={user.email} onChange={handleChange}/>
                    <label htmlFor="phone" className="form-label">Telephone</label>
                    <input className="form-control" type='tel' id="phone" name='phone' value={user.phone} onChange={handleChange}/>
                </div>
                <div className="form-group text-center">
                    <button className="btn btn-warning shadow" type="submit" onClick={handleSubmit}>Modifier</button>
                </div>
            </form>
        </div>
    )

}

export  default EditUser