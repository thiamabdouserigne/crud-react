import React from "react";
import './table.css'

function UserTable(props) {
        return(
            <table>
                <thead> 
                    <th>Prenom</th>
                    <th>Nom</th>
                    <th>Email</th>
                    <th>Telephone</th>
                    <th>Action</th>
                </thead>
                <tbody>
                    {props.users.length > 0 ? (
                        props.users.map(user => {
                            const {id, prenom,nom,email,phone} = user;
                            return(
                                <tr key={id}>
                                    <td>{prenom}</td>
                                    <td>{nom}</td>
                                    <td>{email}</td>
                                    <td>{phone}</td>
                                    <td>
                                    <button className='btn btn-danger' onClick={() => props.deleteUser(id)}>
                                         Supprimer
                                    </button>
                                    </td>
                                    <td>
                                    <button className='btn btn-warning' onClick={() => props.editUser(id,user)}>
                                         Modifier
                                    </button>
                                    </td>
                                    
                                </tr>
                            )
                        })
                    ) : (
                        <tr>
                            <td colSpan={4}>Pas d'utilisateurs trouvés</td>
                        </tr>
                    )}
                </tbody>
            </table>
        )
} 

export default UserTable

